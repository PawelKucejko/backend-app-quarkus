package org.qceyco.rs.v1.controllers;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.qceyco.rs.v1.models.AuthorDto;
import org.qceyco.test.AbstractTest;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
@WithDBData(value = {"testdata.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class AuthorRestControllerTest extends AbstractTest {

    private static final int ZERO = 0;
    private static final int AMOUNT_OF_AUTHORS = 3;
    private static final int AMOUNT_OF_AUTHORS_FOUND_BY_CRITERIA_SEARCH = 1;
    private static final String AUTHOR_ID = "1";
    private static final String AUTHOR_ID_RANDOM = "908734987329732";
    private static final String AUTHOR_ID_DELETED = "3";
    private static final String AUTHOR_FNAME = "Robert";
    private static final String AUTHOR_FNAME_SAVED = "Krzysztof";
    private static final String AUTHOR_FNAME_UPDATED = "Zoltan";
    private static final String AUTHOR_LNAME = "Jordan";
    private static final String AUTHOR_LNAME_SAVED = "Kononowicz";
    private static final String AUTHOR_LNAME_UPDATED = "Smith";
    private static final String AUTHOR_LNAME_RANDOM = "dupadupa";
    private static final String AUTHOR_LNAME_INVALID = " ";

/*
    ####################  GET AUTHOR TESTS ###############################################################################
*/

    @Test
    @DisplayName("Get list of all authors")
    public void shouldReturnListOfAllAuthors() {
        int amountOfAuthorsFound =
                given().
                when().get("/author").
                then().
                        statusCode(OK.getStatusCode()).
                        header(CONTENT_TYPE, APPLICATION_JSON).
                        extract().body().as(getAuthorDtoTypeRef()).getStream().size();
        assertThat(AMOUNT_OF_AUTHORS, equalTo(amountOfAuthorsFound));
    }

    @Test
    @DisplayName("Get empty result when criteria search for author do not match any record")
    public void shouldReturnEmptyResultIfSearchedForNonExistingAuthorByCriteria() {
        int amountOfAuthorsFound =
                given()
                .when().get("/author?lastName=" + AUTHOR_LNAME_RANDOM)
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract().body().as(getAuthorDtoTypeRef()).getStream().size();
        assertThat(ZERO, equalTo(amountOfAuthorsFound));
    }

    @Test
    @DisplayName("Get authors whose first name is indicated in search criteria as '" + AUTHOR_FNAME + "'")
    public void shouldReturnListOfAuthorsByFirstNameCriteria() {
        PageResultDTO<AuthorDto> authors =
                given()
                .when().get("/author?firstName=" + AUTHOR_FNAME)
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract().body().as(getAuthorDtoTypeRef());
        AuthorDto authorFound = authors.getStream().get(0);
        int amountOfAuthorsFound = authors.getStream().size();
        assertThat(AMOUNT_OF_AUTHORS_FOUND_BY_CRITERIA_SEARCH, equalTo(amountOfAuthorsFound));
        assertThat(AUTHOR_ID, equalTo((authorFound.getId())));
        assertThat(AUTHOR_FNAME, equalTo(authorFound.getFirstName()));
        assertThat(AUTHOR_LNAME, equalTo(authorFound.getLastName()));
    }

    @Test
    @DisplayName("Get authors whose last name is indicated in search criteria as '" + AUTHOR_LNAME + "'")
    public void shouldReturnListOfAuthorsByLastNameCriteria() {
        PageResultDTO<AuthorDto> authors =
                given()
                .when().get("/author?lastName=" + AUTHOR_LNAME)
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract().body().as(getAuthorDtoTypeRef());
        AuthorDto authorFound = authors.getStream().get(0);
        int amountOfAuthorsFound = authors.getStream().size();
        assertThat(AMOUNT_OF_AUTHORS_FOUND_BY_CRITERIA_SEARCH, equalTo(amountOfAuthorsFound));
        assertThat(AUTHOR_ID, equalTo((authorFound.getId())));
        assertThat(AUTHOR_FNAME, equalTo(authorFound.getFirstName()));
        assertThat(AUTHOR_LNAME, equalTo(authorFound.getLastName()));
    }

    @Test
    @DisplayName("Get authors whose first and last name is indicated in search criteria as '" + AUTHOR_FNAME + " " + AUTHOR_LNAME + "'")
    public void shouldReturnListOfAuthorsByFirstNameAndLastNameCriteria() {
        PageResultDTO<AuthorDto> authors =
                given()
                .when().get("/author?firstName=" + AUTHOR_FNAME + "&lastName=" + AUTHOR_LNAME)
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getAuthorDtoTypeRef());
        AuthorDto authorFound = authors.getStream().get(0);
        int amountOfAuthorsFound = authors.getStream().size();
        assertThat(AMOUNT_OF_AUTHORS_FOUND_BY_CRITERIA_SEARCH, equalTo(amountOfAuthorsFound));
        assertThat(AUTHOR_ID, equalTo((authorFound.getId())));
        assertThat(AUTHOR_FNAME, equalTo(authorFound.getFirstName()));
        assertThat(AUTHOR_LNAME, equalTo(authorFound.getLastName()));
    }

    @Test
    @DisplayName("Get specific author by id")
    public void shouldReturnAuthorById() {
        given()
        .when().get("/author/" + AUTHOR_ID)
        .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("id", Is.is(AUTHOR_ID))
                .body("firstName", Is.is(AUTHOR_FNAME))
                .body("lastName", Is.is(AUTHOR_LNAME));
    }

    @Test
    @DisplayName("Get error when specific author is not found by id")
    public void shouldReturnNotFoundErrorCodeWhenTryFindAuthorByNotExistingId() {
        given()
        .when().get("/author/" + AUTHOR_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

/*
    ####################  ADD AUTHOR TESTS ###############################################################################
*/

    @Test
    @DisplayName("Get error when added author has invalid last name")
    public void shouldReturnBadRequestStatusWhenTryAddAuthorWithInvalidLastName() {
        AuthorDto addedAuthor = new AuthorDto();
        addedAuthor.setFirstName(AUTHOR_FNAME);
        addedAuthor.setLastName(AUTHOR_LNAME_INVALID);

        given().body(addedAuthor)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().post("/author")
        .then().statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    @DisplayName("Add author when all received data is correct")
    public void shouldAddAuthorWhenDataIsCorrect() {
        AuthorDto authorData = new AuthorDto();
        authorData.setFirstName(AUTHOR_FNAME_SAVED);
        authorData.setLastName(AUTHOR_LNAME_SAVED);

        AuthorDto savedAuthor =
                given().body(authorData)
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .header(ACCEPT, APPLICATION_JSON)
                .when().post("/author")
                .then()
                        .statusCode(CREATED.getStatusCode())
                        .extract()
                        .body().as(AuthorDto.class);

        int amountOfAuthorsInDB =
                given()
                .when().get("/author")
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getAuthorDtoTypeRef()).getStream().size();
        assertThat(AMOUNT_OF_AUTHORS + 1, equalTo(amountOfAuthorsInDB));
        assertThat(savedAuthor.getId(), is(notNullValue()));
        assertThat(savedAuthor.getFirstName(), equalTo(authorData.getFirstName()));
        assertThat(savedAuthor.getLastName(), equalTo(authorData.getLastName()));
    }

    /*
        ####################  UPDATE AUTHOR TESTS ###############################################################################
    */
    @Test
    @DisplayName("Update author when all received data is correct")
    public void shouldUpdateAuthorWhenDataIsCorrect() {
        AuthorDto updatedAuthor = new AuthorDto();
        updatedAuthor.setFirstName(AUTHOR_FNAME_UPDATED);
        updatedAuthor.setLastName(AUTHOR_LNAME_UPDATED);

        given().body(updatedAuthor)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().put("/author/" + AUTHOR_ID)
        .then()
                .statusCode(CREATED.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("id", Is.is(AUTHOR_ID))
                .body("firstName", Is.is(AUTHOR_FNAME_UPDATED))
                .body("lastName", Is.is(AUTHOR_LNAME_UPDATED));

        int amountOfAuthorsInDB =
                given()
                .when().get("/author")
                .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getAuthorDtoTypeRef()).getStream().size();
        assertThat(AMOUNT_OF_AUTHORS, equalTo(amountOfAuthorsInDB));
    }

    @Test
    @DisplayName("Get error when trying to update author with nonexistent id")
    public void shouldReturnNotFoundErrorWhenTryUpdateAuthorWithInvalidId() {
        AuthorDto updatedAuthor = new AuthorDto();
        updatedAuthor.setFirstName(AUTHOR_FNAME_UPDATED);
        updatedAuthor.setLastName(AUTHOR_LNAME_UPDATED);

        given().body(updatedAuthor)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().put("/author/" + AUTHOR_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

    /*
        ####################  DELETE AUTHOR TESTS ###############################################################################
    */
    @Test
    @DisplayName("Delete author when he has no book assigned")
    public void shouldDeleteAuthorWhenNoBookIsAssigned() {
        given()
        .when().delete("/author/" + AUTHOR_ID_DELETED)
        .then().statusCode(CREATED.getStatusCode());

        int amountOfAuthorsInDB =
                given()
                .when().get("/author")
                .then()
                    .statusCode(OK.getStatusCode())
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .extract()
                    .body().as(getAuthorDtoTypeRef()).getStream().size();
        assertThat(AMOUNT_OF_AUTHORS - 1, equalTo(amountOfAuthorsInDB));

    }

    @Test
    @DisplayName("Get error when trying to DELETE author when he has book assigned")
    public void shouldNotDeleteAuthorWhenBookIsAssigned() {
        given()
        .when().delete("/author/" + AUTHOR_ID)
        .then().statusCode(INTERNAL_SERVER_ERROR.getStatusCode());
    }

    @Test
    @DisplayName("Get error when trying to delete author with nonexistent id")
    public void shouldReturnNotFoundErrorWhenTryDeleteAuthorWithInvalidId() {
        given()
        .when().delete("/author/" + AUTHOR_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

    private TypeRef<PageResultDTO<AuthorDto>> getAuthorDtoTypeRef() {
        return new TypeRef<>() {
        };
    }
}
