package org.qceyco.rs.v1.controllers;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.qceyco.rs.v1.models.AuthorDto;
import org.qceyco.rs.v1.models.BookDto;
import org.qceyco.test.AbstractTest;
import org.tkit.quarkus.rs.models.PageResultDTO;
import org.tkit.quarkus.test.WithDBData;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@QuarkusTest
@WithDBData(value = {"testdata.xls"}, deleteBeforeInsert = true, rinseAndRepeat = true)
public class BookRestControllerTest extends AbstractTest {

    private static final int ZERO = 0;
    private static final int AMOUNT_OF_BOOKS = 3;
    private static final int AMOUNT_OF_BOOKS_FOUND_BY_CRITERIA_SEARCH = 1;
    private static final String BOOK_ID = "2";
    private static final String BOOK_ID_RANDOM = "xyz";
    private static final String AUTHOR_ID = "2";
    private static final String AUTHOR_FNAME = "Robert";
    private static final String AUTHOR_LNAME = "Jordan";
    private static final int AUTHOR_VERSION = 0;
    private static final String BOOK_TITLE = "Hospital";
    private static final String BOOK_TITLE_RANDOM = "dupadupa";
    private static final String BOOK_TITLE_INVALID = "";
    private static final String BOOK_TITLE_SAVED = "Added title";
    private static final String BOOK_TITLE_UPDATED = "Updated title";

/*
    ####################  GET BOOK TESTS ###############################################################################
*/
    @Test
    @DisplayName("Get list of all books")
    public void shouldGetAllBooks() {
        PageResultDTO<BookDto> books = given()
                .when().get("/book")
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(getBookDtoTypeRef());
        assertThat(AMOUNT_OF_BOOKS, equalTo(books.getStream().size()));
    }

    @Test
    @DisplayName("Get books whose title is indicated in search criteria as '" + BOOK_TITLE + "'")
    public void shouldReturnListOfBooksByTitleCriteria() {
        PageResultDTO<BookDto> books = given()
                .when().get("/book" + "?title=" + BOOK_TITLE)
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(getBookDtoTypeRef());
        assertThat(AMOUNT_OF_BOOKS_FOUND_BY_CRITERIA_SEARCH, equalTo(books.getStream().size()));
        assertThat(books.getStream().get(0).getId(), equalTo(BOOK_ID));
        assertThat(books.getStream().get(0).getTitle(),equalTo(BOOK_TITLE));
    }

    @Test
    @DisplayName("Get empty result when criteria search for book do not match any record")
    public void shouldReturnEmptyResultIfSearchedForNonExistingBookByCriteria() {
        PageResultDTO<BookDto> books = given()
                .when().get("/book" + "?title=" + BOOK_TITLE_RANDOM)
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(getBookDtoTypeRef());
        assertThat(ZERO, equalTo(books.getStream().size()));
    }

    @Test
    @DisplayName("Get specific book by id")
    public void shouldReturnBookById() {
        given()
        .when().get("/book/" + BOOK_ID)
        .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("id", Is.is(BOOK_ID))
                .body("title", Is.is(BOOK_TITLE));
    }

    @Test
    @DisplayName("Get error when specific book is not found by id")
    public void shouldReturnNotFoundErrorCodeWhenTryFindBookByNotExistingId() {
        given()
        .when().get("/book/" + BOOK_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

/*
    ####################  ADD BOOK TESTS ###############################################################################
*/

    @Test
    @DisplayName("Get error when added book has invalid title")
    public void shouldReturnBadRequestStatusWhenTryAddBookWithInvalidTitle() {
        BookDto addedBook = new BookDto();
        addedBook.setTitle(BOOK_TITLE_INVALID);
        addedBook.setAuthor(new AuthorDto());

        given().body(addedBook)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().post("/book")
        .then().statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    @DisplayName("Get error when added book has no author")
    public void shouldReturnBadRequestStatusWhenTryAddBookWithoutAuthor(){
        BookDto newBook = new BookDto();
        newBook.setTitle(BOOK_TITLE_SAVED);
        newBook.setAuthor(null);

        given()
                .body(newBook)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().post("/book")
        .then().statusCode(BAD_REQUEST.getStatusCode());
    }

    @Test
    @DisplayName("Add book when all received data is correct")
    public void shouldAddBookWhenDataIsCorrect() {
        AuthorDto author = given()
                .when().get("/author/" + AUTHOR_ID)
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(AuthorDto.class);
        BookDto newBook = new BookDto();
        newBook.setTitle(BOOK_TITLE_SAVED);
        newBook.setAuthor(author);

        BookDto savedBook = given()
                .body(newBook)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
                .post("/book")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract()
                .body().as(BookDto.class);

        PageResultDTO books = given()
                .when().get("/book")
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(PageResultDTO.class);

        assertThat(AMOUNT_OF_BOOKS + 1, equalTo(books.getStream().size()));
        assertThat(savedBook.getId(), is(notNullValue()));
        assertThat(savedBook.getTitle(),equalTo(newBook.getTitle()));
        assertThat(savedBook.getAuthor().getId(),equalTo(newBook.getAuthor().getId()));
    }

    /*
        ####################  UPDATE BOOK TESTS ###############################################################################
    */

    @Test
    @DisplayName("Update book when all received data is correct")
    public void shouldUpdateBookWhenDataIsCorrect() {
        AuthorDto author =
                given()
                .when().get("/author/" + AUTHOR_ID)
                .then()
                    .statusCode(OK.getStatusCode())
                    .header(CONTENT_TYPE, APPLICATION_JSON)
                    .extract()
                    .body().as(AuthorDto.class);

        BookDto updatedBook = new BookDto();
        updatedBook.setTitle(BOOK_TITLE_UPDATED);
        updatedBook.setAuthor(author);

        int booksAmount = given()
                .when().get("/book")
                .then()
                .statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .extract()
                .body().as(getBookDtoTypeRef()).getStream().size();

        given()
                .body(updatedBook)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().put("/book/" + BOOK_ID)
        .then()
                .statusCode(CREATED.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("id", Is.is(BOOK_ID))
                .body("title", Is.is(BOOK_TITLE_UPDATED))
                .body("author.id", Is.is(AUTHOR_ID));
        assertThat(AMOUNT_OF_BOOKS, equalTo(booksAmount));
    }

    @Test
    @DisplayName("Get error when trying to update book with nonexistent id")
    public void shouldReturnNotFoundErrorWhenTryUpdateBookWithInvalidId() {
        AuthorDto author = new AuthorDto();
        author.setId(AUTHOR_ID);
        author.setFirstName(AUTHOR_FNAME);
        author.setLastName(AUTHOR_LNAME);
        author.setVersion(AUTHOR_VERSION);
        BookDto updatedBook = new BookDto();
        updatedBook.setTitle(BOOK_TITLE_UPDATED);
        updatedBook.setAuthor(author);

        given()
                .body(updatedBook)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON)
        .when().put("/book/" + BOOK_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

    /*
        ####################  DELETE BOOK TESTS ###############################################################################
    */
    @Test
    @DisplayName("Delete book when id is correct")
    public void shouldDeleteBook() {
        given()
        .when().delete("/book/" + BOOK_ID)
        .then().statusCode(CREATED.getStatusCode());

        int amountOfBooksInDB =
                given()
                        .when().get("/book")
                        .then()
                        .statusCode(OK.getStatusCode())
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .extract()
                        .body().as(getBookDtoTypeRef()).getStream().size();

        assertThat(AMOUNT_OF_BOOKS - 1, equalTo(amountOfBooksInDB));
    }

    @Test
    @DisplayName("Get error when trying to delete book with nonexistent id")
    public void shouldReturnNotFoundErrorWhenTryDeleteBookWithInvalidId() {
        given()
        .when().delete("/book/" + BOOK_ID_RANDOM)
        .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .body("errorCode", Is.is("NOT_FOUND"));
    }

    private TypeRef<PageResultDTO<BookDto>> getBookDtoTypeRef() {
        return new TypeRef<>() {
        };
    }
}
