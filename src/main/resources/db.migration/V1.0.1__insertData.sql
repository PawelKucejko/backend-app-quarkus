DELETE FROM books;
DELETE FROM authors;

INSERT INTO authors (guid,creationdate,creationuser,modificationdate,modificationuser,optlock,first_name,last_name) VALUES ('1','2020-08-12 00:00:00',null,'2020-08-12 00:00:00',null,0,'Henryk','Sienkiewicz');
INSERT INTO authors (guid,creationdate,creationuser,modificationdate,modificationuser,optlock,first_name,last_name) VALUES ('2','2020-08-12 00:00:00',null,'2020-08-12 00:00:00',null,0,'Adam','Mickiewicz');
INSERT INTO books (guid,creationdate,creationuser,modificationdate,modificationuser,optlock,title,author_id) VALUES ('1','2020-08-12 00:00:00',null,'2020-08-12 00:00:00',null,0,'W pustyni i w puszczy','1');
INSERT INTO books (guid,creationdate,creationuser,modificationdate,modificationuser,optlock,title,author_id) VALUES ('2','2020-08-12 00:00:00',null,'2020-08-12 00:00:00',null,0,'Pan Tadeusz','2');
INSERT INTO books (guid,creationdate,creationuser,modificationdate,modificationuser,optlock,title,author_id) VALUES ('3','2020-08-12 00:00:00',null,'2020-08-12 00:00:00',null,0,'Dziady','2');