CREATE TABLE IF NOT EXISTS AUTHORS
(
    GUID             varchar(255) not null,
    creationDate     timestamp,
    creationUser     varchar(255),
    modificationDate timestamp,
    modificationUser varchar(255),
    OPTLOCK          int4         not null,
    FIRST_NAME       varchar(255) not null,
    LAST_NAME        varchar(255) not null,
    primary key (GUID)
);

CREATE TABLE IF NOT EXISTS BOOKS
(
    GUID             varchar(255) not null,
    creationDate     timestamp,
    creationUser     varchar(255),
    modificationDate timestamp,
    modificationUser varchar(255),
    OPTLOCK          int4         not null,
    TITLE            varchar(255) not null,
    AUTHOR_ID        varchar(255) not null,
    primary key (GUID),
    foreign key (AUTHOR_ID) references AUTHORS (GUID)
);
