package org.qceyco.domain.daos;

import org.qceyco.domain.models.Author;
import org.qceyco.domain.models.Author_;
import org.qceyco.domain.models.criteria.AuthorSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class AuthorDao extends AbstractDAO<Author> {

    enum ErrorKeys {
        ERROR_FIND_AUTHORS_BY_CRITERIA,
        ERROR_FIND_AUTHORS_SEARCH_CRITERIA_REQUIRED
    }

    public PageResult<Author> searchByCriteria(AuthorSearchCriteria searchCriteria) {
        if (searchCriteria == null) {
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHORS_SEARCH_CRITERIA_REQUIRED, new NullPointerException());
        }
        try {
            CriteriaQuery<Author> criteriaQuery = criteriaQuery();
            Root<Author> root = criteriaQuery.from(Author.class);
            List<Predicate> predicates = new ArrayList<>();
            CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
            if (searchCriteria.getFirstName() != null && !searchCriteria.getFirstName()
                    .isBlank()) {
                predicates.add(criteriaBuilder.like(root.get(Author_.FIRST_NAME), searchCriteria.getFirstName()));
            }
            if (searchCriteria.getLastName() != null && !searchCriteria.getLastName()
                    .isBlank()) {
                predicates.add(criteriaBuilder.like(root.get(Author_.LAST_NAME), searchCriteria.getLastName()));
            }
            if (!predicates.isEmpty()) {
                criteriaQuery.where(predicates.toArray(new Predicate[0]));
            }
            return createPageQuery(criteriaQuery, Page.of(searchCriteria.getPageNumber(), searchCriteria.getPageSize())).getPageResult();
        } catch (Exception e) {
            throw new DAOException(ErrorKeys.ERROR_FIND_AUTHORS_BY_CRITERIA, e);
        }
    }
}

