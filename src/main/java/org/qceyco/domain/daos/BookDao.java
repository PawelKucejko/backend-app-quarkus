package org.qceyco.domain.daos;

import org.qceyco.domain.models.Author_;
import org.qceyco.domain.models.Book;
import org.qceyco.domain.models.Book_;
import org.qceyco.domain.models.criteria.BookSearchCriteria;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.exceptions.DAOException;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class BookDao extends AbstractDAO<Book> {

    enum ErrorKeys {
        ERROR_FIND_BOOKS_BY_CRITERIA,
        ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED
    }

    public PageResult<Book> searchByCriteria(BookSearchCriteria criteria) {
        if (criteria == null) {
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOKS_SEARCH_CRITERIA_REQUIRED, new NullPointerException());
        }
        try {
            CriteriaQuery<Book> cq = criteriaQuery();
            Root<Book> root = cq.from(Book.class);
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

            List<Predicate> predicates = new ArrayList<>();

            if (criteria.getAuthorLastName() != null && !criteria.getAuthorLastName().isEmpty()) {
                predicates.add(cb.like(cb.lower(root.get(Book_.AUTHOR).get(Author_.LAST_NAME))
                        , criteria.getAuthorLastName().toLowerCase() + "%"));
            }
            if (criteria.getTitle() != null && !criteria.getTitle().isEmpty()) {
                predicates.add(cb.like(cb.lower((root.get(Book_.TITLE)))
                        , criteria.getTitle().toLowerCase() + "%"));
            }
            if (!predicates.isEmpty()) {
                cq.where(predicates.toArray(new Predicate[0]));
            }

            return createPageQuery(cq, Page.of(criteria.getPageNumber(), criteria.getPageSize())).getPageResult();
        } catch (Exception e) {
            throw new DAOException(ErrorKeys.ERROR_FIND_BOOKS_BY_CRITERIA, e);
        }
    }
}