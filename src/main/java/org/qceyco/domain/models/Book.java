package org.qceyco.domain.models;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.*;

@Entity
@Table(name = "BOOKS")
@Getter
@Setter
public class Book extends TraceableEntity {

    @Column(name = "TITLE")
    private String title;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private Author author;

}
