package org.qceyco.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BookSearchCriteria implements Serializable {

    private String authorLastName;

    private String title;

    private Integer pageNumber;

    private Integer pageSize;
}
