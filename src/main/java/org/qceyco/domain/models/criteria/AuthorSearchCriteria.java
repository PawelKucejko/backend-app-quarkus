package org.qceyco.domain.models.criteria;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AuthorSearchCriteria implements Serializable {

    private String firstName;

    private String lastName;

    private Integer pageNumber;

    private Integer pageSize;
}
