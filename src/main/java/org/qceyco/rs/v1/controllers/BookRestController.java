package org.qceyco.rs.v1.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.qceyco.domain.daos.BookDao;
import org.qceyco.domain.models.Book;
import org.qceyco.domain.models.criteria.BookSearchCriteria;
import org.qceyco.rs.v1.mappers.BookMapper;
import org.qceyco.rs.v1.models.BookDto;
import org.qceyco.rs.v1.models.criteria.BookSearchCriteriaDto;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/book")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
@ApplicationScoped
public class BookRestController {

    @Inject
    BookDao bookDao;
    @Inject
    BookMapper bookMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getBookById", description = "Gets book by ID")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDto.class))),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response getBookById(@PathParam("id") String id) throws RestException {
        Book book = bookDao.findById(id);
        if (Objects.nonNull(book)) {
            return Response.status(Response.Status.OK)
                    .entity(bookMapper.mapToBookDto(book))
                    .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }

    @GET
    @Operation(operationId = "getBookByCriteria", description = "Get books by criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding books resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDto.class))),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response findBookByCriteria(@BeanParam BookSearchCriteriaDto searchCriteriaDto) throws RestException {
        BookSearchCriteria criteria = bookMapper.mapToBookSearchCriteria(searchCriteriaDto);
        PageResult<Book> books = bookDao.searchByCriteria(criteria);
        return Response.status(Response.Status.OK)
                .entity(bookMapper.mapToPageResultDTO(books))
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(operationId = "saveBook", description = "Create the book")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Created book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDto.class))),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response saveBook(@Valid BookDto book) throws RestException {
        Book bookEntity = bookMapper.mapToBook(book);
        return Response.status(Response.Status.CREATED).
                entity(bookMapper.mapToBookDto(bookDao.create(bookEntity))).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(operationId = "updateBook", description = "Update the book")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Updated book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDto.class))),
            @APIResponse(responseCode = "404", description = "Not Found"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response updateBook(@PathParam("id") String id, @Valid BookDto bookDto) throws RestException {
        Book bookToUpdate = bookDao.findById(id);
        if (Objects.nonNull(bookToUpdate)) {
            bookMapper.updateBookFromDto(bookDto, bookToUpdate);
            return Response.status(Response.Status.CREATED)
                    .entity(bookMapper.mapToBookDto(bookDao.update(bookToUpdate)))
                    .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }

    @DELETE
    @Path("/{id}")
    @Operation(operationId = "deleteBook", description = "Delete the book")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Deleted book resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = BookDto.class))
            ),
            @APIResponse(responseCode = "404", description = "Not Found"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response deleteBook(@PathParam("id") String id) throws RestException {
        Book bookToDelete = bookDao.findById(id);
        if (Objects.nonNull(bookToDelete)) {
            bookDao.delete(bookToDelete);
            return Response.status(Response.Status.CREATED).build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }
}
