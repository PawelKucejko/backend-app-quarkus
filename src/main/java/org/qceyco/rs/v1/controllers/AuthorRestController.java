package org.qceyco.rs.v1.controllers;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.qceyco.domain.daos.AuthorDao;
import org.qceyco.domain.models.Author;
import org.qceyco.domain.models.criteria.AuthorSearchCriteria;
import org.qceyco.rs.v1.mappers.AuthorMapper;
import org.qceyco.rs.v1.models.AuthorDto;
import org.qceyco.rs.v1.models.criteria.AuthorSearchCriteriaDto;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.exceptions.RestException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/author")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
@ApplicationScoped
public class AuthorRestController {

    @Inject
    AuthorDao authorDao;
    @Inject
    AuthorMapper authorMapper;

    @GET
    @Path("/{id}")
    @Operation(operationId = "getAuthorById", description = "Gets author by ID")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDto.class))
            ),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response getAuthorById(@PathParam("id") String id) throws RestException {
        Author author = authorDao.findById(id);
        if (Objects.nonNull(author)) {
            return Response.status(Response.Status.OK)
                    .entity(authorMapper.mapToAuthorDto(author))
                    .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }

    @GET
    @Operation(operationId = "getAuthorsByCriteria", description = "Get authors by search criteria")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "The corresponding author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDto.class))
            ),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response getAuthorsBySearchCriteria(@BeanParam AuthorSearchCriteriaDto searchCriteriaDto) throws RestException {
        AuthorSearchCriteria criteria = authorMapper.mapToAuthorSearchCriteria(searchCriteriaDto);
        PageResult<Author> authors = authorDao.searchByCriteria(criteria);
        return Response.status(Response.Status.OK)
                .entity(authorMapper.mapToPageResultDTO(authors))
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(operationId = "saveAuthor", description = "Create the author")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Created author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDto.class))),
            @APIResponse(responseCode = "400", description = "Bad request"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response saveAuthor(@Valid AuthorDto author) throws RestException {
        Author authorEntity = authorMapper.mapToAuthor(author);
        return Response.status(Response.Status.CREATED).
                entity(authorMapper.mapToAuthorDto(authorDao.create(authorEntity))).
                build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(operationId = "updateAuthor", description = "Update the author")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Updated author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDto.class))),
            @APIResponse(responseCode = "404", description = "Not Found"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response updateAuthor(@PathParam("id") String id, @Valid AuthorDto author) throws RestException {
        Author authorToUpdate = authorDao.findById(id);
        if (Objects.nonNull(authorToUpdate)) {
            authorMapper.updateAuthorFromDto(author, authorToUpdate);
            return Response.status(Response.Status.CREATED)
                    .entity(authorMapper.mapToAuthorDto(authorDao.update(authorToUpdate)))
                    .build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }

    @DELETE
    @Path("/{id}")
    @Operation(operationId = "deleteAuthor", description = "Delete the author")
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Deleted author resource",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = AuthorDto.class))),
            @APIResponse(responseCode = "404", description = "Not Found"),
            @APIResponse(responseCode = "500", description = "Internal Server Error"),
    })
    public Response deleteAuthor(@PathParam("id") String id) throws RestException {
        Author authorToDelete = authorDao.findById(id);
        if (Objects.nonNull(authorToDelete)) {
            authorDao.delete(authorToDelete);
            return Response.status(Response.Status.CREATED).build();
        }
        throw new RestException(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND);
    }
}
