package org.qceyco.rs.v1.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.qceyco.domain.models.Book;
import org.qceyco.domain.models.criteria.BookSearchCriteria;
import org.qceyco.rs.v1.models.BookDto;
import org.qceyco.rs.v1.models.criteria.BookSearchCriteriaDto;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.models.PageResultDTO;

import java.util.List;

@Mapper(componentModel = "cdi")
public interface BookMapper {

    BookDto mapToBookDto(Book book);

    List<BookDto> mapToListBookDto(List<Book> books);

    Book mapToBook(BookDto bookDto);

    List<Book> mapToListBook(List<BookDto> booksDto);

    BookSearchCriteria mapToBookSearchCriteria(BookSearchCriteriaDto dto);

    PageResultDTO<Book> mapToPageResultDTO(PageResult<Book> page);

    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "creationDate")
    void updateBookFromDto(BookDto bookDto, @MappingTarget Book book);
}
