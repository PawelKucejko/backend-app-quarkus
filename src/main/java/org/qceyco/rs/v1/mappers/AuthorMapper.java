package org.qceyco.rs.v1.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.qceyco.domain.models.criteria.AuthorSearchCriteria;
import org.qceyco.domain.models.Author;
import org.qceyco.rs.v1.models.criteria.AuthorSearchCriteriaDto;
import org.qceyco.rs.v1.models.AuthorDto;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.rs.models.PageResultDTO;

import java.util.List;

@Mapper(componentModel = "cdi")
public interface AuthorMapper {

    AuthorDto mapToAuthorDto(Author author);

    List<AuthorDto> mapToListAuthorDto(List<Author> authors);

    Author mapToAuthor(AuthorDto authorDto);

    List<Author> mapToListAuthor(List<AuthorDto> authorsDto);

    AuthorSearchCriteria mapToAuthorSearchCriteria(AuthorSearchCriteriaDto searchCriteriaDTO);

    PageResultDTO<AuthorDto> mapToPageResultDTO(PageResult<Author> page);

    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "creationDate")
    void updateAuthorFromDto(AuthorDto authorDto, @MappingTarget Author author);
}
