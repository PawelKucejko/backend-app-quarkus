package org.qceyco.rs.v1.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.rs.models.TraceableDTO;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)

public class AuthorDto extends TraceableDTO {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;
}
