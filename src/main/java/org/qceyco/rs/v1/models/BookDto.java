package org.qceyco.rs.v1.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.rs.models.TraceableDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)

public class BookDto extends TraceableDTO {

    @NotBlank
    private String title;

    @NotNull
    private AuthorDto author;
}
