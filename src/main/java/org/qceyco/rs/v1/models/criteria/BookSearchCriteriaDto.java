package org.qceyco.rs.v1.models.criteria;

import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

@Getter
@Setter
public class BookSearchCriteriaDto {

    @QueryParam("title")
    private String title;

    @QueryParam("authorLastName")
    private String authorLastName;

    @QueryParam("page")
    @DefaultValue("0")
    private Integer pageNumber;

    @QueryParam("size")
    @DefaultValue("100")
    private Integer pageSize;
}
