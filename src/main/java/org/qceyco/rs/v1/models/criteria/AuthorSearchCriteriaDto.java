package org.qceyco.rs.v1.models.criteria;

import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

@Getter
@Setter
public class AuthorSearchCriteriaDto {

    @QueryParam("firstName")
    private String firstName;

    @QueryParam("lastName")
    private String lastName;

    @DefaultValue("0")
    @QueryParam("pageNumber")
    private Integer pageNumber;

    @DefaultValue("100")
    @QueryParam("pageSize")
    private Integer pageSize;
}
